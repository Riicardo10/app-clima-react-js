const getClimaIcon = (estado) => {
    switch( estado ) {
        case 'Clouds':  return 'cloud';
        case 'Clear':   return 'day-fog';
        case 'Sun':     return 'day-sunny';
        case 'Rain':    return 'day-rain';
        case 'Windy':   return 'windy';
        case 'Snow':    return 'snow';
        case 'Fog':     return 'day-fog';
        case 'Thunder': return 'thunder';
        case 'Drizzle': return 'drizzle';
        default:        return 'day-sunny';
    }
}

export {getClimaIcon};
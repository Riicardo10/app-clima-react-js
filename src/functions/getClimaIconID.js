const getClimaIconID = (id) => {
    if( id < 300 ) return 'day-thunderstore';
    else if( id < 400 ) return 'day-rain';
    else if( id < 500 ) return 'windy';
    else if( id < 600 ) return 'snow';
    else if( id < 700 ) return 'day-sunny';
    else if( id === 800 ) return 'cloud';
    return 'cloud'
}

export {getClimaIconID};
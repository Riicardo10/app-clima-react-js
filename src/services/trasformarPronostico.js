import moment from 'moment';
import 'moment/locale/es';

const capitalizar = (palabra) => palabra.charAt(0).toUpperCase() + palabra.slice(1).toLowerCase();
const meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

const trasformarPronostico = (data) => (
    data.data.list.filter( item => (
        moment.unix(item.dt).hour() === 6  || 
        moment.unix(item.dt).hour() === 12 ||
        moment.unix(item.dt).hour() === 18
    ) ).map( item => {
        const fecha = (item.dt_txt).split(' ')[0];
        const anio = (fecha).split('-')[0];
        const mes = meses[(fecha).split('-')[1] -1];
        const dia = (fecha).split('-')[2];
        return {
            dia_semana: capitalizar( moment.unix(item.dt).format('dddd') + '' ) + ' ' + dia + ' de ' + mes + ' del ' + anio,
            hora: moment.unix(item.dt).hour(),
            data: {
                temperatura: item.main.temp - 273.15,
                estado: item.weather[0].main,
                id_estado: item.weather[0].id,
                humedad: item.main.humidity,
                viento: item.wind.speed + 'm/s',
            },
        };
    } )
)
    
export default trasformarPronostico;
import {URL, API_KEY} from '../constants/api_url';

const getUrlClimaByCiudad = (ciudad) => {
    return URL + 'q=' + ciudad + '&appid=' + API_KEY + '&units=metric'
}
export default getUrlClimaByCiudad;
import React from 'react';
import Clima from './ClimaLocalizacion';
import PropTypes from 'prop-types';
import './styles.css';

const getComponenteClimaMap = (ciudades, onSelectedLocation) => {
    
    const climaLocationClick = (ciudad) => {
        // console.log('climaLocationClick -> ' + ciudad);
        onSelectedLocation(ciudad);
    }

    return ciudades.map( (ciudad, index) => {
        return ( 
            <Clima 
                key={ciudad} 
                ciudad={ciudad} 
                id={index} 
                climaLocationClick={ () => climaLocationClick(ciudad) } 
            /> 
        )
    }  )
}

const ClimaLista = ( props ) => {
    const {ciudades, onSelectedLocation} = props;
    return (
        <div className='climaLista'>
            { getComponenteClimaMap( ciudades, onSelectedLocation ) }
        </div>
    );
}

ClimaLista.propTypes = {
    ciudades: PropTypes.array.isRequired,
    onSelectedLocation: PropTypes.func,
}

export default ClimaLista;
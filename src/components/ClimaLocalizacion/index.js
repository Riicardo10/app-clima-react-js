// ===== CLASS COMPONENT
import React, {Component} from 'react';
import ClimaLocalizacion from './ClimaLocalizacion';
import ClimaDatos from './ClimaDatos';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
// import LinearProgress from '@material-ui/core/LinearProgress';
import PropTypes from 'prop-types';
import getUrlClimaByCiudad from '../../services/getClimaByCiudad';
// import getClima from '../../services/getClima';
import './styles.css';

class Clima extends Component {

    constructor ( props ) {
        super( props );
        const {ciudad} = props;
        const {id} = props;
        this.state = {
            ciudad,
            data: null,
            id
        };
        //console.log( 'constructor' )
    }

    // < CLICLO DE VIDA DEL COMPONENTE
    componentDidMount() {
        //console.log( 'componentDidMount' );
        this.actualizar();
    }
    componentDidUpdate(prevProps, prevState) {
        //console.log( 'componentDidUpdate' );
    }
    //***> CLICLO DE VIDA DEL COMPONENTE

    actualizar = () => {
        const API_URL = getUrlClimaByCiudad(this.state.ciudad);
        axios.get( API_URL )
            .then( resolve => {
                this.setState( {
                    ciudad: resolve.data.name,
                    data: {
                        temperatura: resolve.data.main.temp,
                        estado: resolve.data.weather[0].main,
                        id_estado: resolve.data.weather[0].id,
                        humedad: resolve.data.main.humidity,
                        viento: resolve.data.wind.speed + 'm/s',
                    },
                } );
            } )
    }
    
    display( ciudad, data, id ){
        if( data != null ){
            return(
                <div style={{ marginBottom: 0 }}>
                    <ClimaLocalizacion ciudad={ciudad} />
                    <ClimaDatos data={data} />
                </div> 
            );
        }
        if(id === 0){
            return (     
                <div style={{marginTop:21}}>
                    <CircularProgress size={100} /> 
                    <h1 style={{color:'#666'}}>Cargando...</h1>
                </div>
            );
        }
    }

    render( props ) {
        const {climaLocationClick} = this.props;
        const {data, id, ciudad} = this.state;
        return (
            <div className='contenedorClima' onClick={climaLocationClick}>
                { this.display(ciudad, data, id) }
            </div>
        )
    }
}
    
Clima.propTypes = {
    ciudad: PropTypes.string.isRequired, 
    climaLocationClick: PropTypes.func,
}

export default Clima;
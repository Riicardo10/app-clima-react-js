import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const ClimaLocalizacion = ( props ) => {
    const {ciudad} = props;
    return ( 
        <div className='contenedorClimaLocalizacion'> 
            <h1 style={{color:'#eee', padding: '0%',}}> {ciudad} </h1> 
        </div> 
    );
}

ClimaLocalizacion.propTypes = {
    ciudad: PropTypes.string.isRequired
};
    
export default ClimaLocalizacion;
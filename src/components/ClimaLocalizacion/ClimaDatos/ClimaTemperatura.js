import React from 'react';
import WheaterIcons from 'react-weathericons';
import {getClimaIconID} from './../../../functions/getClimaIconID';
import PropTypes from 'prop-types';
import './styles.css';

const ClimaTemperatura = ( props ) => {
    const {temperatura, id_estado} = props;
    return (
        <div className='contenedorClimaTemperatura'> 
            <WheaterIcons name={ getClimaIconID(id_estado) } className='wicon' size='2x' /> 
            <span className='temperatura' > {Number(temperatura).toFixed(0)} </span> 
            <span className='temperatura-grados' > ºC </span> 
        </div>
    );
};

ClimaTemperatura.propTypes = {
    temperatura: PropTypes.number.isRequired,
    estado: PropTypes.string.isRequired
};

export default ClimaTemperatura;
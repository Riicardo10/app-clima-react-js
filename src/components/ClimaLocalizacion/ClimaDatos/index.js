import React from 'react';
import PropTypes from 'prop-types';
import ClimaInformacion from './ClimaInformacion';
import ClimaTemperatura from './ClimaTemperatura';
import './styles.css';

const ClimaDatos = ( props ) => {
    const {temperatura, estado, humedad, viento, id_estado} = props.data;
    return (
        <div className='contenedorClimaDatos'>
            <ClimaTemperatura temperatura={temperatura} estado={estado} id_estado={id_estado} />
            <ClimaInformacion humedad={humedad} viento={viento} />
        </div>
    );
}

ClimaDatos.propTypes = {
    props: PropTypes.shape( {
        temperatura: PropTypes.number.isRequired,
        estado: PropTypes.string.isRequired,
        humedad: PropTypes.number.isRequired,
        viento: PropTypes.string.isRequired,
    } )
}

export default ClimaDatos;
import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const ClimaInformacion = (props) => {
    const {humedad, viento} = props;
    return (
        <div className='contenedorClimaInformacion'> 
            <span className='informacion-text'> {`Humedad: ${humedad}% `}</span>  
            <span className='informacion-text'> {`Viento: ${viento}`}</span>  
        </div>
    );
}

ClimaInformacion.propTypes = {
    humedad: PropTypes.number.isRequired,
    viento: PropTypes.string.isRequired
}

export default ClimaInformacion;

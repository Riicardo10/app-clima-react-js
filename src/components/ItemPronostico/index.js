import React from 'react';
import ClimaDatos from '../ClimaLocalizacion/ClimaDatos';
import PropTypes from 'prop-types';

const ItemPronostico = (props) => {
    const {dia_semana, hora, data} = props;
    return (
        <div>
            <p style={{marginBottom: '6%', textAlign:'center'}}> {dia_semana} / Hora: {hora}:00 hrs. </p>
            <ClimaDatos data={data} />
        </div>
    );
}

ItemPronostico.propTypes = {
    dia_semana: PropTypes.string.isRequired,
    hora: PropTypes.number.isRequired,
}

export default ItemPronostico;
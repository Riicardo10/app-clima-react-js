import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './styles.css';
import ItemPronostico from './ItemPronostico';
import trasformarPronostico from '../services/trasformarPronostico';
import axios from 'axios';
import {API_URL_FORECAST, API_KEY} from '../constants/api_url';

class PronosticoExtendido extends Component {
    
    constructor() {
        super();
        this.state = { pronostico: null }
    }

    componentDidMount() {
        this.actualizarCiudad(this.props.ciudad);
    }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.ciudad !== this.props.ciudad){
            this.actualizarCiudad(nextProps.ciudad);
            this.setState({
                pronostico: null
            })
        }
    };
    

    actualizarCiudad  =(ciudad) => {
        const URL = API_URL_FORECAST + '?q=' + ciudad + '&appid=' + API_KEY
        // console.log(URL);
        axios.get( URL )
            .then( res => {
                const pronostico = trasformarPronostico(res);
                this.setState({
                    pronostico
                })
            } );
    }

    renderProgress = () => <h3 style={{textAlign: 'center', color: '#666'}}> Cargando... </h3>
    
    renderItemPronostico(pronostico) {
        return pronostico.map( item => 
            <ItemPronostico key={item.dia_semana + item.hora} dia_semana={item.dia_semana} hora={item.hora} data={item.data} />
        )
    }

    render() {
        const {ciudad} = this.props;
        const {pronostico} = this.state;
        return (
            <div> 
                <h2 className={'titulo-pronostico'}> Pronóstico extendido de {ciudad}: </h2> 
                { pronostico ? this.renderItemPronostico( pronostico ) : this.renderProgress() }
            </div>
        );
    }
}

PronosticoExtendido.propTypes = {
    ciudad: PropTypes.string.isRequired,
}

export default PronosticoExtendido;
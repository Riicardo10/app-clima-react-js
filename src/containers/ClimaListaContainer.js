import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setCiudad} from '../actions';
import ClimaLista from '../components/ClimaLista';

class ClimaListaContainer extends Component {

    selectedLocation = (ciudad) => {
        // console.log('selectedLocation-> ' + ciudad)
        this.props.dispatchSetCiudad( ciudad );
    }

    render() {
        return (
            <ClimaLista ciudades={this.props.ciudades} onSelectedLocation={this.selectedLocation} />
        );
    }
}

ClimaListaContainer.propTypes = {
    dispatchSetCiudad: PropTypes.func.isRequired,
    ciudades: PropTypes.array.isRequired,
};

const mapDispatchToProps = (dispatch) => ( {
    dispatchSetCiudad: value => dispatch(setCiudad(value))
} )

export default connect(null, mapDispatchToProps)(ClimaListaContainer)
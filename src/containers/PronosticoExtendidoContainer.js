import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import PronosticoExtendido from '../components/PronosticoExtendido';

class PronosticoExtendidoContainer extends Component {
    render() {
        return (
            this.props.ciudad &&
            <PronosticoExtendido ciudad={this.props.ciudad} />
        );
    }
}

PronosticoExtendidoContainer.propTypes = {
    ciudad: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
    return { ciudad: state.ciudad };
}

export default connect(mapStateToProps, null)(PronosticoExtendidoContainer);
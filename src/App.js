import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import './App.css';
import {Grid, Row, Col} from 'react-flexbox-grid';
// import PronosticoExtendido from './components/PronosticoExtendido';
import ClimaListaContainer from './containers/ClimaListaContainer';
import PronosticoExtendidoContainer from './containers/PronosticoExtendidoContainer';

const ciudades = ['Washington,us','Buenos Aires,ar','Bogota,col','Mexico,mex','Barcelona,es','Guadalajara,mex','Guerrero,mex','Ohio,us','Denver,us','Chicago,us','Boston,us','Lima,pe','Madrid,es','Ciudad de México,mex'];

class App extends Component { 
    render() {
        return (
            <Grid>
                <Row> <h1 className='titulo'> LISTA DE CLIMAS </h1> </Row>
                <Row>
                    <Col xs={12} md={6}>
                        <ClimaListaContainer ciudades={ciudades} />
                    </Col>
                    <Col xs={12} md={6}>
                        <Paper elevation={5}>
                            <div className='detalles'>
                                { <PronosticoExtendidoContainer/> }
                            </div>
                        </Paper>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default App;
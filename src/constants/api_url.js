export const URL = 'https://openweathermap.org/data/2.5/weather?';
const LOCATION = 'Buenos Aires,ar';
export const API_KEY = 'b6907d289e10d714a6e88b30761fae22';
// export const API_KEY = 'f99bbd9e4959b513e9bd0d7f7356b38d';

export const API_URL = URL + 'q=' + LOCATION + '&appid=' + API_KEY + '&units=metric';

// export const API_URL_FORECAST = 'https://samples.openweathermap.org/data/2.5/forecast';
export const API_URL_FORECAST = 'https://samples.openweathermap.org/data/2.5/forecast';
//export const ppp = 'https://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b6907d289e10d714a6e88b30761fae22';

